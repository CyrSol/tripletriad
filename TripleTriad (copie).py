import pygame
import sys
import os
import datetime

sys.path.append("..")
from cards.card_game import *
from cards.card_manager import *
from cards.card_stats import *
from card_AITRT import *
from cards.card_display import *

from gameWindow.GameWindowTK import *

#Define sizes
#-----------------------
SCREEN_WIDTH=790
SCREEN_HEIGHT=540
CARD_WIDTH = 120
CARD_HEIGHT = 156
#-----------------------

class CardTRT(Card):
	""" card's attribut"""
	
	def __init__(self,name,rank,suit,value,tag,up,left,down,right):
		Card.__init__(self,name,rank,suit,value,tag)
		self.up=up
		self.down=down
		self.left=left
		self.right=right
		self.b_player = 0
		self.i = -1
		self.j = -1
	


	def turnAround(self,player):
		suffix=""
		if "2" not in self.name : suffix="2"
		self.name = self.tag+suffix
		self.b_player = player
	
	def getInfo(self):
		#return self.tag + "(" + str(self.j) + "x" + str(self.i) +")" + " N:" + str(self.up) + " O:" + str(self.left) + " S:" + str(self.down) + " E:" + str(self.right) 
		return str(self.rank).zfill(2) + " - " + self.tag + " N:" + str(self.up) + " O:" + str(self.left) + " S:" + str(self.down) + " E:" + str(self.right) 

#ls TripleTriad/* | awk '{print split($0,a,".") ,split(a[1],b,"/"),split(b[2],c,"2"),c[1],b[2],$0}' | awk '{print $4,",",$5,",0,0,0,0,0,triple_triad_ff8,",$6}' > TRT_cards.csv


def loadTRTCards(file_name):
	fic = open(file_name,'r')
	cards = []
	basePath = os.path.dirname(__file__)
	print(file_name)
	for line in fic :
		val = line.rstrip('\n').split(",")
		if len(val) ==  8:
			nameFic = val[1].strip()
			name = val[1].strip()
			up=int(val[3])
			left=int(val[4])
			down=int(val[5])
			right=int(val[6])
			print(name+" -> "+ basePath + nameFic)
			card = CardTRT(name,int(val[2]),val[7],up+down+left+right,val[0].strip(),up,left,down,right)
			cards.append(card)
			
	#cards = [c for c in cards if c.rank>=8]
	return cards

class Rule(object):
	def __init__(self, name):
		self.name = name

	def applyRule(self,card,up,left,down,right):
		return None,None,None

class RuleClassic(Rule):
	def __init__(self):
		self.name = "Classique"

	def applyRule(self,card,up,left,down,right):
		cardsList = []
		if up is not None and up.name!=""  : cardsList.append(self.check_card(up,up.down,card,card.up))
		if left is not None and left.name!=""  : cardsList.append(self.check_card(left,left.right,card,card.left))
		if down is not None and down.name!=""  : cardsList.append(self.check_card(down,down.up,card,card.down))
		if right is not None and right.name!=""  : cardsList.append(self.check_card(right,right.left,card,card.right))
					
		return self.name,list(filter(None, cardsList)) ,0

	def check_card(self,card_checked,card_checked_value,card,card_value):
		if card_checked_value < card_value and card_checked.b_player != card.b_player :
			#print(self.name + " : " + card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			return card_checked
		else:
			return None

class RuleCombo(RuleClassic):
	def __init__(self):
		self.name = "Combo"

	def applyRule(self,card,up,left,down,right):
		name,cardsList ,propa = RuleClassic.applyRule(self,card,up,left,down,right)
		return self.name,cardsList,1			

class RulePlus(Rule):
	def __init__(self):
		self.name = "Plus"

	def applyRule(self,card,up,left,down,right):
		cardsList = []
		if up is not None and up.name!="" and left is not None and left.name!="" :
			cardsList.append(self.check_card(up,up.down+card.up,card,card.left+left.right))
		if up is not None and up.name!="" and right is not None and right.name!="" : 
			cardsList.append(self.check_card(up,up.down+card.up,card,card.right+right.left))
		if up is not None and up.name!="" and down is not None and down.name!="" : 
			cardsList.append(self.check_card(up,up.down+card.up,card,card.down+down.up))

		if left is not None and left.name!="" and up is not None and up.name!=""  : 
			cardsList.append(self.check_card(left,left.right+card.left,card,card.up+up.down))
		if left is not None and left.name!=""  and down is not None and down.name!="" : 
			cardsList.append(self.check_card(left,left.right+card.left,card,card.down+down.up))
		if left is not None and left.name!=""  and right is not None and right.name!="" : 
			cardsList.append(self.check_card(left,left.right+card.left,card,card.right+right.left))


		if down is not None and down.name!="" and left is not None and left.name!="" : 
			cardsList.append(self.check_card(down,down.up+card.down,card,card.left+left.right))
		if down is not None and down.name!="" and right is not None and right.name!="" : 
			cardsList.append(self.check_card(down,down.up+card.down,card,card.right+right.left))
		if down is not None and down.name!="" and up is not None and up.name!="" : 
			cardsList.append(self.check_card(down,down.up+card.down,card,card.up+up.down))

		if right is not None and right.name!="" and up is not None and up.name!=""   : 
			cardsList.append(self.check_card(right,right.left+card.right,card,card.up+up.down))
		if right is not None and right.name!="" and down is not None and down.name!="" : 
			cardsList.append(self.check_card(right,right.left+card.right,card,card.down+down.up))
		if right is not None and right.name!="" and left is not None and left.name!="" : 
			cardsList.append(self.check_card(right,right.left+card.right,card,card.left+left.right))

		return self.name,list(set(list(filter(None, cardsList)))) ,1

	def check_card(self,card_checked,card_checked_value,card,card_value):
		if card_checked_value == card_value and card_checked.b_player != card.b_player :
			#print(self.name + " : " + card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			return card_checked
		else:
			return None

class RuleDouble(RulePlus):

	def __init__(self):
		self.name = "Double"


	def check_card(self,card_checked,card_checked_value,card,card_value):
		if (card_checked_value == 2*card_value or card_checked_value*2 == card_value) and card_checked.b_player != card.b_player :
			#print(self.name + " : " + card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			return card_checked
		else:
			return None

class RuleIdentique(Rule):
	def __init__(self):
		self.name = "Identique"

	def applyRule(self,card,up,left,down,right):
		cardsList = []
		if up is not None and up.name!="" and left is not None and left.name!="" :
			cardsList.append(self.check_card(up,up.down-card.up,card,card.left-left.right))
		if up is not None and up.name!="" and right is not None and right.name!="" : 
			cardsList.append(self.check_card(up,up.down-card.up,card,card.right-right.left))
		if up is not None and up.name!="" and down is not None and down.name!="" : 
			cardsList.append(self.check_card(up,up.down-card.up,card,card.down-down.up))

		if left is not None and left.name!="" and up is not None and up.name!=""  : 
			cardsList.append(self.check_card(left,left.right-card.left,card,card.up-up.down))
		if left is not None and left.name!=""  and down is not None and down.name!="" : 
			cardsList.append(self.check_card(left,left.right-card.left,card,card.down-down.up))
		if left is not None and left.name!=""  and right is not None and right.name!="" : 
			cardsList.append(self.check_card(left,left.right-card.left,card,card.right-right.left))

		if down is not None and down.name!="" and left is not None and left.name!="" : 
			cardsList.append(self.check_card(down,down.up-card.down,card,card.left-left.right))
		if down is not None and down.name!="" and right is not None and right.name!="" : 
			cardsList.append(self.check_card(down,down.up+card.down,card,card.right-right.left))
		if down is not None and down.name!="" and up is not None and up.name!="" : 
			cardsList.append(self.check_card(down,down.up-card.down,card,card.up-up.down))

		if right is not None and right.name!="" and up is not None and up.name!=""   : 
			cardsList.append(self.check_card(right,right.left-card.right,card,card.up-up.down))
		if right is not None and right.name!="" and down is not None and down.name!="" : 
			cardsList.append(self.check_card(right,right.left-card.right,card,card.down-down.up))
		if right is not None and right.name!="" and left is not None and left.name!="" : 
			cardsList.append(self.check_card(right,right.left-card.right,card,card.left-left.right))

		return self.name,list(set(list(filter(None, cardsList)))) ,1

	def check_card(self,card_checked,card_checked_value,card,card_value):
		if card_checked_value == 0 and card_value == 0 and card_checked.b_player != card.b_player :
			#print(self.name + " : " + card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			return card_checked
		else:
			return None


class GameTRT(Game):
	
	def __init__(self,general_params,game_params):
		Game.__init__(self,general_params,game_params)
		self.awaited = []
		self.decksTab = []
		for i in range(0,3):
			deck = Deck("Deck" + str(i))
			self.decksTab.append(deck)
			self.decks.append(deck)
			self.interfacedDecks.append(InterfacedDeckDescriptor(deck,deck.deckType))
		
		self.stack = []
		self.player=0
		self.rules = []
		self.rules_combo = []
		self.global_rules= []
		#self.tab = [[None,None,None],[None,None,None],[None,None,None]]


	def initDecksTab(self):
		for d in self.decksTab :
			card = emptyCard()
			card.hidden = False
			d.addBack(card)
			card = emptyCard()
			card.hidden = False
			d.addBack(card)
			card = emptyCard()
			card.hidden = False
			d.addBack(card)
			

	def start(self):
		self.state = Game.INITIALISATION
		for pl in self.players:
			pl.selected = []
		self.gameInitialisation()
	
	def step(self):
		if(not self.isGameOver()):

			if self.state == Game.PREPARATION and len(self.awaited) == 0 :
				for pl in self.players:
					pl.current_score = 0
					self.awaited.append(pl.name)
				self.changed=True

			elif self.state == Game.DISTRIBUTION:

				self.board.fillStock()
				self.board.shuffle()
				print ("Distrib len : " + str(len(self.board.stock_init.cards)))
				for pl in self.players:
					pl.current_score = 0

				for i in range (0,5):
					for pl in self.players:	
						card = self.board.draw()
						print ("Distrib : " + card.getInfo())
						card.hidden = "Ouvert" not in self.global_rules and (self.players.index(pl) != 0)
						if (self.players.index(pl) != 0) : card.turnAround(1)
						pl.addBack(card)
						pl.current_score = pl.current_score + 1
				
				self.initDecksTab()
				self.state = Game.SELECTION
				self.awaited.append(self.players[self.player].name)
				self.changed=True
				
			elif self.state == Game.CONFIRMATION:
				self.state = Game.ACTION
				card = self.players[self.player].selected[0]
				card.marked = False
				self.players[self.player].selected = []
				self.awaited.remove(self.players[self.player].name)
				#card.hidden = False
				self.selected = [(card,0)]
				#self.stack = sorted(self.stack,key=lambda item: item[1].rank)
				self.changed=True
					
			#elif self.state == Game.ACTION and self.chrono.check():
			elif self.state == Game.ACTION:
				player = self.players[(self.player)]
				player2 = self.players[(self.player+1) % len(self.players)]

				if(len(self.selected) > 0):
					card,propagation = self.selected[0]
					#print ("Action : " + card.getInfo())
					up,left,down,right = self.getCards(card.j,card.i)
					
					current_rules=[]
					if propagation==0:
						current_rules = self.rules
					else:
						current_rules = self.rules_combo
					i=0
					cardsList = []
					propa=0
					for i in range(0,len(current_rules)):
						#print ("Règle : " + current_rules[i].name)
						name,cardsList,propa = current_rules[i].applyRule(card,up,left,down,right)
						if(len(cardsList)) > 0:
							break

					if i >= len(current_rules)-1:
						self.selected.remove((card,propagation))
					
					for c in cardsList:
						c.hidden = True
						self.stack.append(c)
						if propa:
							self.selected.append((c,1))
							#print ("Propa : " + c.getInfo())
							
				
				if (len(self.stack)>0):
					self.state = Game.PROPAGATION

				elif  (len(self.selected) == 0):
					self.player = (self.player + 1) % 2	
					if len(player.cards) == 0:
						if player.current_score == player2.current_score:
							for d in self.decksTab:
								for c in d.cards:
									self.players[c.b_player].addBack(c)
								d.cards = []	
							self.initDecksTab()
							self.state = Game.SELECTION
							self.awaited.append(self.players[self.player].name)
						else:
							for pl in self.players:
								pl.stats[-1].game_played = 1
								pl.stats[-1].score = pl.current_score
								if pl.stats[-1].score > 5:
									pl.stats[-1].game_won = 1
									pl.victory = pl.victory + 1

							if(self.nb_games == self.game_count):
								ficName=  self.general_params["log_file"] + str(datetime.datetime.now())
								printStats(ficName,self.players)
								self.state = Game.GAME_OVER
							else:
								self.state = Game.VALIDATION
								self.game_count=self.game_count+1
								for p in self.players :
									self.awaited.append(p.name)
							
					else :
						self.awaited.append(self.players[self.player].name)
						self.state = Game.SELECTION	
				self.changed=True
				
			elif self.state == Game.PROPAGATION:
				player = self.players[(self.player)]
				player2 = self.players[(self.player+1) % len(self.players)]
				for c in self.stack:
					c.turnAround(self.player)
					player.current_score = player.current_score +1
					player2.current_score = player2.current_score - 1
					c.hidden=False
				self.stack= []
				self.state = Game.ACTION
				self.changed=True

			elif self.state == Game.INITIALISATION:
				self.state = Game.DISTRIBUTION
				
				for p in self.players :
					p.cards=[]
				for d in self.decksTab:
					d.cards = []

	def action(self, message,player,deck,card):
		
		Game.action(self, message,player,deck,card)
		if(not  self.isGameOver()):
			if(player.name in self.awaited):
				
				if (message.messageType == Deck.PLAYER) and  self.state == Game.PREPARATION :
					player.current_score = len(player.cards)
					print(player.name + " a choisi")
					self.changed=True
					self.awaited.remove(player.name)
					if (len(self.awaited) == 0):
						#print("Distribution go")
						self.state = Game.DISTRIBUTION
					self.changed=True
					
				if (message.messageType == Deck.PLAYER) and player.name == self.players[self.player].name and player.name == deck.name and self.state == Game.SELECTION :
					#card.hidden = not card.hidden	
					for c in deck.cards:
						c.marked = False
					card.marked = True 
					player.selected = [card]
					self.changed=True
				

				if (message.messageType == Deck.BOARD) and self.state == Game.SELECTION and len(player.selected) == 1:
					if(card.name == ""):
						indexI = deck.cards.index(card)
						indexJ = self.decksTab.index(deck)					
						#deck.selected.append(card)
						#player.selected[0].marked = False
						player.removeCard(player.selected[0])
						player.selected[0].hidden = False
						self.state = Game.CONFIRMATION
						#self.chrono.raz()
						self.decksTab[indexJ].cards[indexI]= player.selected[0]
						player.selected[0].i = indexI
						player.selected[0].j = indexJ
						self.changed=True
					else:
						self.board.info = "Veuillez choisir un emplacement vide"
						self.changed=True

				#elif(message.messageType == Deck.BOARD)  and self.state == Game.EXPECTATION:
				#	self.awaited.remove(player.name)
				#	p, card = self.stack[0]
				#	self.pickUp(p,deck)
				#	self.throwCard(p,deck, card)
				#	self.state = Game.ACTION
				#	self.changed=True

				elif(message.messageType == Deck.BOARD)  and self.state == Game.VALIDATION:
					self.awaited.remove(player.name)
					if (len(self.awaited) == 0):
						self.state = Game.INITIALISATION
						
				elif(message.messageType == Deck.CHOICE) :
					print("CHOICE : " + message.to_string())
				#self.board.info = "A vous de jouer"

	def getCards(self,j,i):
		moveup=j-1
		moveleft=i-1
		movedown=j+1
		moveright=i+1
		up=None
		left=None
		down=None
		right=None

		#print("u " + str(moveup)+ " : " + str(self.i))
		#print("l " + str(self.j)+ " : " + str(moveleft))
		#print("d " + str(movedown)+ " : " + str(self.i))
		#print("r " + str(self.j)+ " : " + str(moveright))

		if moveup >= 0 : up= self.decksTab[moveup].cards[i]
		if moveleft >= 0 : left= self.decksTab[j].cards[moveleft]
		if movedown < len(self.decksTab) : down= self.decksTab[movedown].cards[i]
		if moveright < len(self.decksTab[j].cards) : right= self.decksTab[j].cards[moveright]

		return up,left,down,right

	def check_card(self,card_checked,card_checked_value,card,card_value):
		if card_checked_value < card_value and card_checked.b_player != card.b_player :
			#print(card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			card_checked.hidden = True
			self.stack.append(card_checked)

	def free_rooms(self) :
		listing=[]
		for j in range(0,len(self.decksTab)) :
			for  i in range(0,len(self.decksTab[j].cards)) :
				if self.decksTab[j].cards[i].name == "" :
					listing.append((j,i))
		return listing

	def pointsExposed(self,b_player):
		sumPoints=0
		nbPoints=0
		for j in range(0,len(self.decksTab)):
			for i in range(0,len(self.decksTab[j].cards)):
				c=self.decksTab[j].cards[i]
				#print("Card :-> " + c.getInfo())
				if (c.name!="" and c.b_player == b_player) :
					sumPointsx=0
					nbPointsx=0
					up,left,down,right =  self.getCards(j,i)
					if (up is not None and up.name==""):
						sumPointsx = sumPointsx + c.up
						nbPointsx = nbPointsx+1
					if (left is not None and left.name==""):
						sumPointsx = sumPointsx + c.left
						nbPointsx = nbPointsx+1
					if (down is not None and down.name==""):
						sumPointsx = sumPointsx + c.down
						nbPointsx = nbPointsx+1
					if (right is not None and right.name==""):
						sumPointsx = sumPointsx + c.right
						nbPointsx = nbPointsx+1
					sumPoints = sumPoints + sumPointsx
					nbPoints = nbPoints + nbPointsx
					#print("Card :-> " + c.getInfo()  + " " + str(nbPointsx) + " " + str(sumPointsx))
		
		return sumPoints,nbPoints

	def loadInterfacedDecks(self):
		self.interfacedDecks.append(InterfacedDeckDescriptor(self.players[0],self.players[0].deckType))
		self.interfacedDecks.append(InterfacedDeckDescriptor(self.players[1],self.players[1].deckType))
	
	def loadRules(self):
		# Rules
		self.rules = [RuleDouble(),RulePlus(),RuleIdentique(),RuleClassic()]
		self.rules_combo = [RuleCombo()]
		#game.global_rules.append("Ouvert")


class GameFactoryTRT(GameFactory):
	
	def getGame(self):
		return GameTRT(self.general_params,self.game_params)

class UITRTManager(object):
	
	def __init__(self,width,height):
		UIManager.__init__(self,width,height)
	
	def display(self,game,screen):
		UIManager.display(self,game,screen)

	def displayPlayers(self,players,screen):

		if len(players) == 2:
			font = pygame.font.Font(None,80)
			text = font.render(str(players[0].current_score),True,WHITE)
			screen.blit(text,[701,480])
			text = font.render(str(players[1].current_score),True,WHITE)
			screen.blit(text,[75,480])
			
			font = pygame.font.Font(None,20)
			text = font.render(str(players[0].victory),True,WHITE)
			screen.blit(text,[701,10])
			text = font.render(str(players[1].victory),True,WHITE)
			screen.blit(text,[75,10])

	def displayInfo(self,game,screen):
		font = pygame.font.Font(None,15)
		text = font.render("Partie",True,WHITE)
		screen.blit(text,[self.width-60,2])
		text = font.render(str(game.game_count),True,WHITE)
		screen.blit(text,[self.width-20,2])

class UIManagerFactoryTRT():
	def getUIManager(self,w,h): 
		return UITRTManager(w,h)


def main():
	
	general_params=loadParams("general_params.json")
	game_params=loadParams(general_params["game_params"])
	
	#display(general_params,game_params,cards,GameManagerFactoryTRT(),UIManagerFactoryTRT())
	GameWindowTK.display(general_params,game_params,GameManagerFactory(GameFactoryTRT(general_params,game_params),AIFactoryTRT(),loadTRTCards))

if __name__ == '__main__':
	#debug_display_cards(loadTRTCards)
	main()

	

