import pygame
import sys
import os
import datetime
sys.path.append("//home//xol//Pygame")
from cards.card_game import *
from cards.card_manager import *
from cards.card_stats import *
from cards.card_AI import *


#Define sizes
#-----------------------
SCREEN_WIDTH=790
SCREEN_HEIGHT=540
STOCK_X=210
STOCK_Y=10
WASTE_X=300
WASTE_Y=30
STOCK_MARGE=0
WASTE_MARGE=0
FONT_SIZE=25
CARD_WIDTH = 120
CARD_HEIGHT = int(CARD_WIDTH*1.3)
#-----------------------

class CardTRT(Card):
	""" card's attribut"""
	
	def __init__(self,name,rank,suit,value,tag,up,left,down,right):
		Card.__init__(self,name,rank,suit,value,tag)
		self.up=up
		self.down=down
		self.left=left
		self.right=right
		self.b_player = 0
	
	def turnAround(self,player):
		suffix=""
		if "2" not in self.name : suffix="2"
		self.name = self.tag+suffix
		self.b_player = player
	
	def getInfo(self):
		return self.tag + " N:" + str(self.up) + " O:" + str(self.left) + " S:" + str(self.down) + " E:" + str(self.right) 

#ls TripleTriad/* | awk '{print split($0,a,".") ,split(a[1],b,"/"),split(b[2],c,"2"),c[1],b[2],$0}' | awk '{print $4,",",$5,",0,0,0,0,0,triple_triad_ff8,",$6}' > TRT_cards.csv

def loadTRTCards(file_name):
	fic = open(file_name,'r')
	cards = []
	card_dict = { }
	basePath = os.path.dirname(__file__)
	print(file_name)
	for line in fic :
		val = line.rstrip('\n').split(",")
		if len(val) ==  9:
			nameFic = val[8].strip()
			name = val[1].strip()
			up=int(val[3])
			left=int(val[4])
			down=int(val[5])
			right=int(val[6])
			print(name+" -> "+ basePath + nameFic)
			card = CardTRT(name,int(val[2]),val[7],up+down+left+right,val[0].strip(),up,left,down,right)
			imagePath = os.path.join(basePath, nameFic)
			img = pygame.image.load(imagePath).convert()
			if "2" not in card.name : cards.append(card)
			card_dict[name] = img
	imagePathBack = os.path.join(basePath, "TripleTriad/01_back.png")
	img = pygame.image.load(imagePathBack).convert()
	card_dict["back"] = img

	return cards, card_dict

class GameTRT(Game):
	
	def __init__(self):
		Game.__init__(self)
		self.awaited = []
		self.decksTab = []
		for i in range(0,3):
			deck = Deck("Deck" + str(i))
			self.decksTab.append(deck)
			self.decks.append(deck)
		self.stack = []
		self.player=0
		self.i=-1
		self.j=-1
		#self.tab = [[None,None,None],[None,None,None],[None,None,None]]

	def emptyCard(self):
		return Card("",0,"",0,"")

	def initDecksTab(self):
		for d in self.decksTab :
			card = self.emptyCard()
			card.hidden = False
			d.addBack(card)
			card = self.emptyCard()
			card.hidden = False
			d.addBack(card)
			card = self.emptyCard()
			card.hidden = False
			d.addBack(card)

	def start(self):
		self.state = Game.DISTRIBUTION
		self.gameInitialisation()
	
	def step(self):
		if(not self.isGameOver()):
			if self.state == Game.DISTRIBUTION:
				self.round_count=self.round_count+1
				self.board.shuffle()

				for pl in self.players:
					pl.current_score = 0

				for i in range (0,5):
					for pl in self.players:	
						card = self.board.draw()
						card.hidden = (self.players.index(pl) != 0)
						if (self.players.index(pl) != 0) : card.turnAround(1)
						pl.addBack(card)
						pl.current_score = pl.current_score + 1
			
				self.initDecksTab()
				self.state = Game.SELECTION
				self.awaited.append(self.players[self.player].name)
				self.changed=True
				
			elif self.state == Game.CONFIRMATION:
				self.state = Game.ACTION
				card = self.players[self.player].selected[0]
				card.marked = False
				self.players[self.player].selected = []
				self.awaited.remove(self.players[self.player].name)
				#card.hidden = False
				self.selected = [card]
				#self.stack = sorted(self.stack,key=lambda item: item[1].rank)
				self.changed=True
					
			#elif self.state == Game.ACTION and self.chrono.check():
			elif self.state == Game.ACTION:
				player = self.players[(self.player)]
				player2 = self.players[(self.player+1) % len(self.players)]

				if(len(self.selected) > 0):
					card = self.selected[0]
					up,left,down,right = self.getCards(self.j,self.i)
					
					if up is not None and up.name!=""  : self.check_card(up,up.down,card,card.up)
					if left is not None and left.name!=""  : self.check_card(left,left.right,card,card.left)
					if down is not None and down.name!=""  : self.check_card(down,down.up,card,card.down)
					if right is not None and right.name!=""  : self.check_card(right,right.left,card,card.right)
						
					self.selected = []
					
				
				
				
				if (len(self.stack)>0):
					self.state = Game.EXPECTATION
				else :
					self.player = (self.player + 1) % 2	
					if len(player.cards) == 0:
						if player.current_score == player2.current_score:
							for d in self.decksTab:
								for c in d.cards:
									self.players[c.b_player].addBack(c)
								d.cards = []	
							self.initDecksTab()
							self.state = Game.SELECTION
							self.awaited.append(self.players[self.player].name)
						else:
							self.state = Game.DISTRIBUTION
							self.game_count = self.game_count + 1
							player2.cards=[]
							for d in self.decksTab:
								d.cards = []
					else :
						self.awaited.append(self.players[self.player].name)
						self.state = Game.SELECTION	
				self.changed=True
				
			elif self.state == Game.EXPECTATION:
				player = self.players[(self.player)]
				player2 = self.players[(self.player+1) % len(self.players)]
				for c in self.stack:
					c.turnAround(self.player)
					player.current_score = player.current_score +1
					player2.current_score = player2.current_score - 1
					c.hidden=False
				self.stack= []
				self.state = Game.ACTION
				self.changed=True

	def action(self, message,player,deck,card):
		
		Game.action(self, message,player,deck,card)
		if(not  self.isGameOver()):
			if(player.name in self.awaited):
				
					
				if (message.messageType == Deck.PLAYER) and player == deck and self.state == Game.SELECTION :
					#card.hidden = not card.hidden	
					for c in deck.cards:
						c.marked = False
					card.marked = True 
					player.selected = [card]
					self.board.info = card.getInfo()
					self.changed=True
				

				if (message.messageType == Deck.BOARD) and self.state == Game.SELECTION and len(player.selected) == 1:
					if(card.name == ""):
						indexI = deck.cards.index(card)
						indexJ = self.decksTab.index(deck)					
						#deck.selected.append(card)
						#player.selected[0].marked = False
						player.removeCard(player.selected[0])
						player.selected[0].hidden = False
						self.state = Game.CONFIRMATION
						#self.chrono.raz()
						self.decksTab[indexJ].cards[indexI]= player.selected[0]
						self.i = indexI
						self.j = indexJ
						self.changed=True
					else:
						self.board.info = "Veuillez choisir un emplacement vide"
						self.changed=True

				elif(message.messageType == Deck.BOARD)  and self.state == Game.EXPECTATION:
					self.awaited.remove(player.name)
					p, card = self.stack[0]
					self.pickUp(p,deck)
					self.throwCard(p,deck, card)
					self.state = Game.ACTION
					self.changed=True
					
				#self.board.info = "A vous de jouer"

	def getCards(self,j,i):
		moveup=j-1
		moveleft=i-1
		movedown=j+1
		moveright=i+1
		up=None
		left=None
		down=None
		right=None

		#print("u " + str(moveup)+ " : " + str(self.i))
		#print("l " + str(self.j)+ " : " + str(moveleft))
		#print("d " + str(movedown)+ " : " + str(self.i))
		#print("r " + str(self.j)+ " : " + str(moveright))

		if moveup >= 0 : up= self.decksTab[moveup].cards[i]
		if moveleft >= 0 : left= self.decksTab[j].cards[moveleft]
		if movedown < len(self.decksTab) : down= self.decksTab[movedown].cards[i]
		if moveright < len(self.decksTab[j].cards) : right= self.decksTab[j].cards[moveright]

		return up,left,down,right

	def check_card(self,card_checked,card_checked_value,card,card_value):
		if card_checked_value < card_value and card_checked.b_player != card.b_player :
			print(card_checked.name + " " + str(card_checked.b_player) + " " + str(card_checked_value)+ " vs " + str(card_value) + " " + str(card.b_player) + " " + card.name)
			card_checked.hidden = True
			self.stack.append(card_checked)

class InterfaceGameTRTManagerFactory(object):

	def getInterfaceGameManager(self,game):
		interfaceGameManager = InterfaceGameManager()
		i=0
		for d in game.decksTab :
			deckIterface = DeckInterface(STOCK_X,STOCK_Y+i,CARD_WIDTH,CARD_HEIGHT,CARD_WIDTH+5,d.name,Deck.BOARD,DeckInterface.HORIZONTAL,DeckDisplayManager(d),True)
			interfaceGameManager.addDeckInterface(deckIterface)
			i=i+CARD_HEIGHT+20

		playerInterface = DeckInterface(650,25,CARD_WIDTH,CARD_HEIGHT,CARD_HEIGHT-int(CARD_HEIGHT/2),game.players[0].name,Deck.PLAYER,DeckInterface.VERTICAL,DeckDisplayManager(game.players[0]),True)
		interfaceGameManager.addDeckInterface(playerInterface)
		
		playerInterface2 = DeckInterface(25,25,CARD_WIDTH,CARD_HEIGHT,CARD_HEIGHT-int(CARD_HEIGHT/2),game.players[1].name,Deck.PLAYER,DeckInterface.VERTICAL,DeckDisplayManager(game.players[1]),True)
		interfaceGameManager.addDeckInterface(playerInterface2)
			
		
		return interfaceGameManager

class UITRTManager(object):
	
	def __init__(self,width,height):
		UIManager.__init__(self,width,height)
	
	def display(self,game,screen):
		UIManager.display(self,game,screen)

	def displayPlayers(self,players,screen):
		font = pygame.font.Font(None,80)
		if len(players) == 2:
			text = font.render(str(players[0].current_score),True,WHITE)
			screen.blit(text,[700,480])
			text = font.render(str(players[1].current_score),True,WHITE)
			screen.blit(text,[75,480])
			

	def displayInfo(self,game,screen):
		font = pygame.font.Font(None,20)
		text = font.render("Partie",True,WHITE)
		screen.blit(text,[self.width-110,5])
		text = font.render(str(game.game_count),True,WHITE)
		screen.blit(text,[self.width-50,5])
def main():
	pygame.init()

		
	# screen and game #
	# Set the width and height of the screen [width, height]
	screen = pygame.display.set_mode([SCREEN_WIDTH,SCREEN_HEIGHT])
	pygame.display.set_caption("Triple Triad FF8")
	cards, card_dict = loadTRTCards("TRT_cards.csv")
	basePath = os.path.dirname(__file__)
	imagePathBkg = os.path.join(basePath, "TripleTriad/02_tapis_triple_triad.jpg")
	imgBkg = pygame.image.load(imagePathBkg).convert()
	picture = pygame.transform.scale(imgBkg, (SCREEN_WIDTH, SCREEN_HEIGHT-15))
	game = GameTRT()
	game.nb_games = 100

	tick = 20
	reflexionTime = 1000


	# Players #
	player = Player("player")
	game.addPlayer(player)
	eventManager = EventManager(player.name)
	aiManager = AIManager()
	playerAI = PlayerAI("ia1",AI6QPRandom())
	#aiManager.addPlayer(playerAI)
	#game.addPlayer(playerAI)
	playerAI2 = PlayerAI("ia2",AITRTRandom())
	aiManager.addPlayer(playerAI2)
	game.addPlayer(playerAI2)
	
	game.fullAI = (game.getNbPlayers() == aiManager.getNbPlayers)
	if game.fullAI:
		#game.chrono = ChronoPy(0)
		tick = 1000
		reflexionTime = 0
			

	interfaceGameManager = InterfaceGameTRTManagerFactory().getInterfaceGameManager(game)
	interfaceManager = InterfaceManager()
	interfaceGameManager.interface(interfaceManager)
	gameManager = GameManager(game,interfaceManager,cards,eventManager,aiManager,reflexionTime)
	uiManager = UITRTManager(SCREEN_WIDTH,SCREEN_HEIGHT)
	backgroundManager = BackgroundManager(SCREEN_WIDTH,SCREEN_HEIGHT,picture,(0,128,128))
	gameManager.initialisation()
	#interfaceGameManager.update(game)
	""" fin int game Manager """
	#Loop until the user clicks the close button.
	done = False

	# Used to manage how fast the screen updates
	clock = pygame.time.Clock()
	#-------------------------------------------
	
	
	# -------- Main Program Loop -----------
	while not done:
		# --- Main event loop
		ret = gameManager.step()
		if game.isGameOver():
			tick = 20
		if ret == GameManager.QUIT:
			done = True

		# --- Game logic should go here
		gameManager.refresh(screen,card_dict,interfaceGameManager,uiManager,backgroundManager)
		pygame.display.flip()

		# --- Limit to 20 frames per second
		clock.tick(tick)

	# Close the window and quit.
	# If you forget this line, the program will 'hang'
	# on exit if running from IDLE.
	pygame.quit()

if __name__ == '__main__':
	main()
