import random
from cards.card_game import *
from cards.card_AI import *
from cards.card_stats import *
from operator import itemgetter
from copy import *


class AIFactoryTRT():
	def getAI(self,num):
		if num == 1 : return  AITRTRandom()
		elif num == 2 : return AITRTMiniMax2()
		elif num ==0 : return AITRTHuman()
		else : return None

##########################################################################################################
##########################################################################################################

class AITRTHuman(AI):
	def __init__(self):
		AI.__init__(self,0,'AI_TRT_Human')

	def play(self,game,player):
		if game.state == Game.PREPARATION :
			#cardsList = listCardsOwned(player)
			#for i in range(0,5):
				#print(i)
			#	e = cardsList.pop(random.randint(0,len(cardsList)-1))
				#print(e)
			#	c = game.board.getCardFromName(e)
			#	player.addBack(c)
			if len(player.cards) == 0:
				player.cardsPlayable = listCardsOwned(player,game.board.getCompl())
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,0)
		
			
class AITRTRandom(AI):
	def __init__(self):
		AI.__init__(self,0,'AI_TRT_Random')

	def play(self,game,player):
		#print(player.name + " -> " + str(game.state) + ";" + str(len(player.cards)))
			
		if game.state == Game.SELECTION and len(player.selected) == 0:
			if len(player.cards) > 0 :
				index = random.randrange(0,len(player.cards))
				return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,index)
			
		elif game.state == Game.PREPARATION and len(player.cards) == 0:
			cardsList = listCardsOwned(player)
			for i in range(0,5):
				c = game.board.getCardFromName(getCardName(cardsList.pop(random.randint(0,len(cardsList)-1))))
				
				player.addBack(c)
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,0)
			
		elif game.state == Game.SELECTION and len(player.selected) == 1:
			listing = game.free_rooms()
			j,i = listing[random.randint(0,len(listing)-1)]
			#index = random.randint(0,len(game.decksTab)-1)
			#i = random.randint(0,len(game.decksTab[index].cards)-1)
			#print(str(j) + ":" + str(i))
			return MessageDeck(Message.GAME,Deck.BOARD,player.name,game.decksTab[j].name,i)

		elif game.state == Game.VALIDATION:
			return MessageDeck(Message.GAME,Deck.BOARD,	player.name,game.decksTab[0].name,0)

class AITRTMiniMax(AI):	
	def __init__(self):
		AI.__init__(self,0,'AI_TRT_MiniMax')
		self.i=-1
		self.j=-1

	def play(self,game,player):
		#print(player.name + " -> " + str(game.state) + ";" + str(len(player.cards)))
			
		if game.state == Game.SELECTION and len(player.selected) == 0:
			self.i=-1
			self.j=-1
			score = player.score
			listing = game.free_rooms()
			current_player = game.player
			scores=[]
			for c in player.cards:
				for k,l in listing:
					self.i = l
					self.j = k
					#print(c.name + "->" +str(k) + ":" + str(l) + "/" + str(len(player.selected)))
					index = player.cards.index(c)
					game_simul = deepcopy(game)
					game_simul.sendMessage(MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,index))
					game_simul.readMessages()
					game_simul.step()
					while(current_player == game_simul.player):
						#print(c.name + "-->" +str(k) + ":" + str(l) + "/" + str(len(player.selected)))
						if(player.name in game_simul.awaited):
							m = game_simul.getPlayer(player.name).play(game_simul)
							#print(m.to_string())
							game_simul.sendMessage(m)
							game_simul.readMessages()
						game_simul.step()
					scores.append((index,k,l,game_simul.getPlayer(player.name).current_score))
			best_moves=[]
			score_max = max(scores,key=itemgetter(3))[3] 
			#print(scores)
			#print("Score max :" + str(score_max))
			for index,k,l,s in scores:
				if s == score_max :
					best_moves.append((index,k,l))
			index,k,l= best_moves[0]
			self.i = l
			self.j = k
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,index)

					
			
		elif game.state == Game.PREPARATION and len(player.cards) == 0:
			cardsList = listCardsOwned(player)
			for i in range(0,5):
				c = game.board.getCardFromName(getCardName(cardsList.pop(random.randint(0,len(cardsList)-1))))
				
				player.addBack(c)
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,0)
			
		elif game.state == Game.SELECTION and len(player.selected) == 1:
			#index = random.randint(0,len(game.decksTab)-1)
			#i = random.randint(0,len(game.decksTab[index].cards)-1)
			#print(str(j) + ":" + str(i))
			return MessageDeck(Message.GAME,Deck.BOARD,player.name,game.decksTab[self.j].name,self.i)

		elif game.state == Game.VALIDATION:
			return MessageDeck(Message.GAME,Deck.BOARD,player.name,game.decksTab[0].name,0)	

class AITRTMiniMax2(AI):
	def __init__(self):
		AI.__init__(self,0,'AI_TRT_MiniMax2')
		self.i=-1
		self.j=-1

	def play(self,game,player):
		#print(player.name + " -> " + str(game.state) + ";" + str(len(player.cards)))
			
		if game.state == Game.SELECTION and len(player.selected) == 0:
			self.i=-1
			self.j=-1
			score = player.score
			listing = game.free_rooms()
			#print(listing)
			current_player = game.player
			scores=[]
			for c in player.cards:
				for k,l in listing:
					self.i = l
					self.j = k
					#print(c.name + "->" +str(k) + ":" + str(l) + "/" + str(len(player.selected)))
					index = player.cards.index(c)
					game_simul = deepcopy(game)
					game_simul.sendMessage(MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,index))
					game_simul.readMessages()
					game_simul.step()
					while(current_player == game_simul.player):
						#print(c.name + "-->" +str(k) + ":" + str(l) + "/" + str(len(player.selected)))
						if(player.name in game_simul.awaited):
							m = game_simul.getPlayer(player.name).play(game_simul)
							game_simul.sendMessage(m)
							#print(m.to_string())
							game_simul.readMessages()
						game_simul.step()
					sumPoints, nbPoints = game_simul.pointsExposed(current_player)
					note=game_simul.getPlayer(player.name).current_score*100
					#print (str(sumPoints)+ "x" + str(nbPoints))
					if nbPoints == 0:
						note = note+11
					else:
						note=note+int(sumPoints/nbPoints)
					scores.append((index,k,l,note))
			best_moves=[]
			score_max = max(scores,key=itemgetter(3))[3] 
			#print(scores)
			#print("Score max :" + str(score_max))
			for index,k,l,s in scores:
				if s == score_max :
					best_moves.append((index,k,l,s))
			index,k,l,s = best_moves[random.randint(0,len(best_moves)-1)]
			self.i = l
			self.j = k
			print(best_moves)
			print("Choix :" + str(index) + "->" + str(self.j) + ":" + str(self.i))
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,index)

					
			
		elif game.state == Game.PREPARATION and len(player.cards) == 0:
			cardsList = listCardsOwned(player)
			for i in range(0,5):
				e = getCardName(cardsList.pop(random.randint(0,len(cardsList)-1)))
				#print(e)
				c = game.board.getCardFromName(e)
				
				player.addBack(c)
			return MessageDeck(Message.GAME,Deck.PLAYER,player.name,player.name,0)
			
		elif game.state == Game.SELECTION and len(player.selected) == 1:
			#index = random.randint(0,len(game.decksTab)-1)
			#i = random.randint(0,len(game.decksTab[index].cards)-1)
			#print(str(j) + ":" + str(i))
			return MessageDeck(Message.GAME,Deck.BOARD,player.name,game.decksTab[self.j].name,self.i)

		elif game.state == Game.VALIDATION:
			return MessageDeck(Message.GAME,Deck.BOARD,player.name,game.decksTab[0].name,0)	

